package org.sda.jsp.util;

import org.junit.Test;
import org.sda.jsp.dto.User;

import static org.junit.Assert.assertTrue;

public class UserValidationHibernateTest {
    @Test
    public void validate() throws Exception {
        //given
        User user = new User("Jan", "Kowalski", 123456789, true);
        //when
        boolean isOk = ValidationUtil.validate(user);
        //then
        assertTrue(isOk);
    }

    @Test
    public void isNotOkValidateFirstName() throws Exception {
        //given
        User user = new User("", "Kowalski", 123456789, true);
        //when
        boolean isOk = ValidationUtil.validate(user);
        //then
        assertTrue(!isOk);
    }

    @Test
    public void isNotOkValidatePhone() throws Exception {
        //given
        User user = new User("Jan", "Kowalski", 23456789, true);
        //when
        boolean isOk = ValidationUtil.validate(user);
        //then
        assertTrue(!isOk);
    }

}