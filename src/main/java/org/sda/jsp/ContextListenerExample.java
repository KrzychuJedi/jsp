package org.sda.jsp;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoDb;
import org.sda.jsp.dao.UserDaoInMemory;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

@WebListener
public class ContextListenerExample implements ServletContextListener {

    @Resource(name = "jdbc/sda_DB")
    DataSource ds;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        IUserDao userDao = new UserDaoDb(ds);
        ServletContext cntxt = servletContextEvent.getServletContext();
        cntxt.setAttribute("userDao", userDao);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
