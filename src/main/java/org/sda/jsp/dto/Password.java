package org.sda.jsp.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Password {

    @NotNull
    @NotEmpty
    private String password;

    @NotNull
    private User user;

    private int id;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
