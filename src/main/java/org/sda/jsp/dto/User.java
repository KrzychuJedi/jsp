package org.sda.jsp.dto;

import org.hibernate.validator.constraints.CreditCardNumber;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.sda.jsp.validation.EnsureNumber;

import javax.validation.constraints.*;
import java.io.Serializable;

public class User implements Serializable {

    private int id;

    @NotNull
    @NotEmpty
    private String login;

    @NotNull
    @Length(max = 20)
    @NotEmpty
    private String firstName;

    @NotNull
    @Length(max = 20)
    @NotEmpty
    private String lastName;

    @EnsureNumber(message = "Wrong number")
    private Integer phone;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private boolean present;

    public User() {
    }

    public User(String firstName, String lastName, Integer phone, boolean present) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.present = present;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public boolean isPresent(){
        return this.present;
    }
    public void setPresent(boolean present){
        this.present = present;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone=" + phone +
                ", present=" + present +
                '}';
    }
}
