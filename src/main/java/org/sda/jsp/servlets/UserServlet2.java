package org.sda.jsp.servlets;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoInMemory;
import org.sda.jsp.dto.User;
import org.sda.jsp.util.ValidationUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.*;

@WebServlet("/user2")
public class UserServlet2 extends CommonServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        String name = request.getParameter("name");
        String lastName = request.getParameter("lastname");
        String login = request.getParameter("login");
        int phone = Integer.valueOf(request.getParameter("phone"));
        Boolean present = Boolean.valueOf(request.getParameter("present"));
        user.setFirstName(name);
        user.setLastName(lastName);
        user.setPhone(phone);
        user.setPresent(present);
        user.setLogin(login);

//        boolean correct = UserValidation.validate(user);
        Set<ConstraintViolation<User>> constraintViolations = ValidationUtil.validateInternal(user);
        if(constraintViolations.isEmpty()) {
            userDao.save(user);
            forwadToUserPage(request, response);
        }
        else {
            Iterator<ConstraintViolation<User>> iterator = constraintViolations.iterator();
            Map<String,String> messageList = new HashMap();
            while (iterator.hasNext()) {
                ConstraintViolation<User> tmp = iterator.next();
               // messageList.add(tmp.getMessage());
                messageList.put(tmp.getPropertyPath().toString(),tmp.getMessageTemplate());
            }
            request.setAttribute("errorList",messageList);
            forwadToUserAdd(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwadToUserPage(req,resp);
    }

/*
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User toEditUser = userDao.getById(Integer.valueOf(req.getParameter("id")));
        String name = req.getParameter("name");
        String lastName = req.getParameter("lastname");
        int phone = Integer.valueOf(req.getParameter("phone"));
        Boolean present = Boolean.valueOf(req.getParameter("present"));

        toEditUser.setFirstName(name);
        toEditUser.setLastName(lastName);
        toEditUser.setPhone(phone);
        toEditUser.setPresent(present);

        userDao.update(toEditUser);
        forwadToUserPage(req,resp);
    }
*/

}
