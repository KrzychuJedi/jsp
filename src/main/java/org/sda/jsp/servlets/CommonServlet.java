package org.sda.jsp.servlets;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoInMemory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CommonServlet extends HttpServlet {

    IUserDao userDao;

    @Override
    public void init() throws ServletException {
        super.init();
        userDao = (IUserDao) getServletContext().getAttribute("userDao");
    }

    public void forwadToUserPage(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        req.setAttribute("userList", userDao.findAll());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/user-table.jsp");
        requestDispatcher.forward(req, resp);
    }


    public void forwadToUserAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/adduser.jsp");
        requestDispatcher.forward(req, resp);
    }

    public void forwadToRegistration(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/registration.jsp");
        requestDispatcher.forward(req, resp);
    }

    public void forwadToLoginServlet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/login");
        requestDispatcher.forward(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
        userDao = null;
    }
}
