package org.sda.jsp.servlets;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoInMemory;
import org.sda.jsp.dto.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deleteuser")
public class DeleteUser extends CommonServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User userToDelete = userDao.getById(Integer.valueOf(req.getParameter("id")));
        userDao.delete(userToDelete);
        forwadToUserPage(req,resp);
    }

}
