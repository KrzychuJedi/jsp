package org.sda.jsp.servlets;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoInMemory;
import org.sda.jsp.dto.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/user")
public class UserServlet extends CommonServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        String name = req.getParameter("name");
        String lastName = req.getParameter("name");
        int phone = Integer.valueOf(req.getParameter("phone"));
        Boolean present = Boolean.valueOf(req.getParameter("present"));
        user.setFirstName(name);
        user.setLastName(lastName);
        user.setPhone(phone);
        user.setPresent(present);

        userDao.save(user);
        printUser(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        printUser(req,resp);

    }

    public void printUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out = resp.getWriter();

        out.println("<html><body>");
        out.println("<h2>Lits of user</h2>");
        List<User> users = userDao.findAll();
        String test;
        for (int i = 0; i < users.size(); i++) {
            out.println(users.get(i).toString());
            //2 metoda
//            test = users.get(i).getFirstName();
        }
        out.println("</body></html>");
    }

    @Override
    public void destroy() {
        super.destroy();
        userDao = null;
    }
}
