package org.sda.jsp.servlets;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoInMemory;
import org.sda.jsp.dto.Password;
import org.sda.jsp.dto.User;
import org.sda.jsp.util.PasswordUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends CommonServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        Password dbPassword = userDao.getPasswordByLogin(login);
        if (PasswordUtil.checkPassword(password, dbPassword.getPassword())) {
            req.getSession().setAttribute("logedUser", new User());
            forwadToUserPage(req,resp);
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/userlogin.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
}
