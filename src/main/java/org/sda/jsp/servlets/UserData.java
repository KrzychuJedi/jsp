package org.sda.jsp.servlets;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoInMemory;
import org.sda.jsp.dto.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/userdata")
public class UserData extends CommonServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.valueOf(req.getParameter("id"));
        User user = userDao.getById(id);
        req.setAttribute("user",user);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/edit-user.jsp");
        requestDispatcher.forward(req, resp);
    }
}
