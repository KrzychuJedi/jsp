package org.sda.jsp.servlets;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoInMemory;
import org.sda.jsp.dto.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user-edit")
public class UserEdit extends HttpServlet {

    IUserDao userDao;

    @Override
    public void init() throws ServletException {
        super.init();

        userDao = new UserDaoInMemory();
    }

    public void forwadToUserPage(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.setAttribute("userList", userDao.findAll());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/user-table.jsp");

        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User toEditUser = userDao.getById(Integer.valueOf(req.getParameter("id")));
        String name = req.getParameter("name");
        String lastName = req.getParameter("lastname");
        int phone = Integer.valueOf(req.getParameter("phone"));
        Boolean present = Boolean.valueOf(req.getParameter("present"));

        toEditUser.setFirstName(name);
        toEditUser.setLastName(lastName);
        toEditUser.setPhone(phone);
        toEditUser.setPresent(present);

        userDao.update(toEditUser);
        forwadToUserPage(req,resp);
    }

    @Override
    public void destroy() {
        super.destroy();
        userDao = null;
    }
}
