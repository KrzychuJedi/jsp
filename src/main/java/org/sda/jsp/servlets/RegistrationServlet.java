package org.sda.jsp.servlets;

import org.sda.jsp.dao.IUserDao;
import org.sda.jsp.dao.UserDaoInMemory;
import org.sda.jsp.dto.Password;
import org.sda.jsp.dto.User;
import org.sda.jsp.util.PasswordUtil;
import org.sda.jsp.util.ValidationUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@WebServlet("/registeruser")
public class RegistrationServlet extends CommonServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = new User();
        Password pass = new Password();

        String name = request.getParameter("name");
        String lastName = request.getParameter("lastname");
        int phone = Integer.valueOf(request.getParameter("phone"));
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        user.setFirstName(name);
        user.setLastName(lastName);
        user.setPhone(phone);
        user.setLogin(login);
        pass.setPassword(PasswordUtil.hashPassword(password));

        pass.setUser(user);

        Set<ConstraintViolation<User>> constraintViolations = ValidationUtil.validateInternal(user);
        Set<ConstraintViolation<Password>> constraintViolationsPass = ValidationUtil.validateInternal(pass);
        if(constraintViolations.isEmpty() && constraintViolationsPass.isEmpty()) {
            userDao.save(user, pass);
            forwadToLoginServlet(request, response);
        }
        else {
            Iterator<ConstraintViolation<User>> iterator = constraintViolations.iterator();
            Map<String,String> messageList = new HashMap();
            while (iterator.hasNext()) {
                ConstraintViolation<User> tmp = iterator.next();
                // messageList.add(tmp.getMessage());
                messageList.put(tmp.getPropertyPath().toString(),tmp.getMessageTemplate());
            }
            request.setAttribute("errorList",messageList);
            forwadToRegistration(request, response);
        }
    }

}
