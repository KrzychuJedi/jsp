package org.sda.jsp.util;

import org.sda.jsp.dto.Password;
import org.sda.jsp.dto.User;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class ValidationUtil {

    private static Validator validator;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    public static boolean validate(User user) {
        Set<ConstraintViolation<User>> constraintViolations = validateInternal(user);
        return constraintViolations.isEmpty();
    }

    public static <T> Set<ConstraintViolation<T>> validateInternal(T entity) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(entity);
        return constraintViolations;
    }
}
