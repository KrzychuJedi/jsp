package org.sda.jsp.util;

import org.sda.jsp.dto.User;

public class UserValidation {

    public static boolean validate(User user){
       return elementaryValidate(user.getFirstName())
       && elementaryValidate(user.getLastName())
       && user.getPhone().toString().length() == 9;
    }

    private static boolean elementaryValidate(String name) {
        return !(name.equals(null)
                || name.equals("")
                || name.length() > 20);
    }


}
