package org.sda.jsp.dao;

import org.sda.jsp.dto.Password;
import org.sda.jsp.dto.User;
import org.sda.jsp.util.DbUtil;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoDb implements IUserDao {

    private final DataSource ds;

    public UserDaoDb(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public User save(User user) {
        try {
            Connection con = ds.getConnection();
            String sql = "INSERT INTO user(login,first_name,last_name,phone) VALUES (?,?,?,?)";
            PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,user.getLogin());
            statement.setString(2,user.getFirstName());
            statement.setString(3,user.getLastName());
            statement.setString(4,user.getPhone().toString());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            rs.next();
            user.setId(rs.getInt(1));
            DbUtil.close(con,statement,null);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        }
        return user;
    }

    @Override
    public void save(User user, Password password) {
        try {
            save(user);
            Connection con = ds.getConnection();
            String sql = "INSERT INTO password(password,user_id) VALUES (?,?)";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1,password.getPassword());
            statement.setInt(2,user.getId());
            statement.executeUpdate();
            DbUtil.close(con,statement,null);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        }
    }



    @Override
    public Password getPasswordByLogin(String login) {
        return null;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try {
            Connection con = ds.getConnection();
            String select = "select * from user";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(select);
            while (rs.next()) {
                User user = new User();
                user.setLogin(rs.getString("login"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setPhone(rs.getInt("phone"));
                user.setId(rs.getInt("id"));
                users.add(user);
            }
            DbUtil.close(con,stmt,rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }return users;
    }

    @Override
    public User getByLogin(String login) {
        return null;
    }

    @Override
    public User getById(int id) {
        return null;
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void delete(User user) {

    }
}
