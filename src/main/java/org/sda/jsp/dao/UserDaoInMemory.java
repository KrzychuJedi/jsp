package org.sda.jsp.dao;

import org.sda.jsp.dto.Password;
import org.sda.jsp.dto.User;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class UserDaoInMemory implements IUserDao {

    private static List<User> users = new ArrayList<>();
    private static List<Password> passwords = new ArrayList<>();

    private static int count = 0;

    @Override
    public User save(User user){
        user.setId(count++); // baza danych ustawia nam id
        users.add(user);
        return user;
    };

    @Override
    public void save(User user, Password password){
        save(user);
        password.setId(count++);
        passwords.add(password);
    }

    @Override
    public User getByLogin(String login){
        return users.stream().filter(user -> user.getLogin().equals(login)).findFirst().get();
    }

    @Override
    public Password getPasswordByLogin(String login){
        return passwords.stream().filter(password -> password.getUser().getLogin().equals(login)).findFirst().get();
    }

    @Override
    public List<User> findAll(){
        return users;
    }

    @Override
    public User getById(int id) {
        return users.stream().filter(user -> user.getId() == id).findFirst().get();

        /*
        for(User user: users){
            if(user.getId() == id){
                return user;
            }
        }*/
    }
    //nie potrzeba implementacji ze względu na referencje
    @Override
    public void update(User user){
        //noop
    }

    @Override
    public void delete(User user) {
        users.remove(user);
    }


    public void test(){

    }
}
