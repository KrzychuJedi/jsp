package org.sda.jsp.dao;

import org.sda.jsp.dto.Password;
import org.sda.jsp.dto.User;

import java.util.List;

public interface IUserDao {

    User save(User user);

    void save (User user, Password password);

    Password getPasswordByLogin(String login);

    List<User> findAll();


    User getByLogin(String login);

    User getById(int id);

    //nie potrzeba implementacji ze względu na referencje
    void update(User user);

    void delete(User user);
}
