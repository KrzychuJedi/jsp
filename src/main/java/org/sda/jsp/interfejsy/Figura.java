package org.sda.jsp.interfejsy;

public interface Figura {

    String przedstawSie();

    double policzPole();

    double policzObwod();
}
