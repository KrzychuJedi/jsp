package org.sda.jsp.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//@WebFilter("/*")
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);

        List<String> whiteList = new ArrayList<>();
        whiteList.add("/userlogin.jsp");
        whiteList.add("/login");
        whiteList.add("/registration.jsp");
        whiteList.add("/registeruser");

        if(!whiteList.contains(request.getRequestURI())){
            if(request.getSession().getAttribute("logedUser")!=null){
                chain.doFilter(req, res);
            }else{
                response.sendRedirect("/userlogin.jsp");
//            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/userlogin.jsp");
//            requestDispatcher.forward(request, response);
            }
        }
        else{
            chain.doFilter(req, res);
        }





    }

    @Override
    public void destroy() {

    }
}
