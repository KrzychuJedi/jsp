package org.sda.jsp;

import org.sda.jsp.interfejsy.*;

import java.util.ArrayList;
import java.util.List;

public class Test {

    private static class Dog {
        public String voice;

        public String getVoice() {
            return voice;
        }

        public void setVoice(String voice) {
            this.voice = voice;
        }
    }

    public static void main(String[] args) {
        Dog pimpus = new Dog();
        pimpus.setVoice("Hau Hau!");
        test(pimpus);
        System.out.println(pimpus.getVoice());

        Figura figura1 = new Prostokat(2, 3);
        Figura figura2 = new Kolo(3);

        List<Figura> figury = new ArrayList<>();
        figury.add(figura1);
        figury.add(figura2);

        for (Figura figura : figury) {
            System.out.println(figura.przedstawSie());
            System.out.println(figura.policzPole());
            System.out.println(figura.policzObwod());
            System.out.println("------");
        }

        for (int i = 0; i < figury.size(); i++) {
            System.out.println(figury.get(i).przedstawSie());
            System.out.println(figury.get(i).policzPole());
            System.out.println(figury.get(i).policzObwod());
            System.out.println("------");
        }

        figury.forEach(figura -> {
            System.out.println(figura.przedstawSie());
            System.out.println(figura.policzPole());
            System.out.println(figura.policzObwod());
        });
    }

    public static void test(Dog dog) {
        dog.setVoice("Miau!");
//        dog = new Dog();
//        dog.setVoice("Miau!");
    }
}
