<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add User</title>

    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"/>--%>

    <%--<c:set var="locale" value="${param.language}"></c:set>--%>
    <c:set var="locale" value="${not empty param.language ?
          param.language : 'pl_PL'}" scope="session" />
    <fmt:setLocale value="${locale}"/>
    <fmt:setBundle basename="org.sda.jsp.labels"/>



</head>
<body>

<form action="/user2" method="post">
    Login <input type="text" name="login" required>
    <br>
    Name <input type="text" name="name" required>
    <br>
    Lastname <input type="text" name="lastname" required>
    <br>
    Phone <input type="number" name="phone" required>
    <br>
    Present
    yes <input type="radio" name="present" value="true">
    no <input type="radio" name="present" value="false">
    <button type="submit">Send</button>


</form>

<br>
<ul>

<c:forEach items="${errorList}" var="error">
    <li>
        ${error.key}
            <%--${error}--%>
        <fmt:message key = "${error.value}"></fmt:message>
    </li>
</c:forEach>
</ul>
</body>
</html>
