<%@ page import="org.sda.jsp.dto.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    List<User> users = new ArrayList<>();
    users.add(new User("Jan", "Kowalski", 880506999, true));
    users.add(new User("Dobromir", "Kowalski", 880506999, true));

    pageContext.setAttribute("users", users);
%>

<html>
<head>
    <title>Title</title>
</head>
<body>
<c:forEach items="${users}" var="user">
    <t:userdetail user="${user}"/>
    <hr>
</c:forEach>
</body>
</html>
