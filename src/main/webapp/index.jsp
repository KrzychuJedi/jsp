<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.time.LocalTime" %>
<%@ page import="java.util.Enumeration" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="org.sda.jsp.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="org.sda.jsp.util.JspUtil" %>

<%--<c:set var="locale" value="${param.language}"></c:set>--%>
<c:set var="locale" value="${not empty param.language ?
          param.language : 'pl_PL'}" scope="session" />
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="org.sda.jsp.labels"/>

<%
    List<String> list2 = new ArrayList<String>();
    list2.add("Polska");
    list2.add("Czechy");
    list2.add("Rosja");
    list2.add("Włochy");
    pageContext.setAttribute("list2",list2);
%>
<%!
    LocalTime localTime;

    // TODO dodaj kod który pobierze godzinę o której JSP jest zainicjalizowane
    public void jspInit() {
        localTime = LocalTime.now();
    }

    // TODO wyświetl informację o której godzinie była inicjalizacja i jaka jest godzina teraz

    public void jspDestroy() {
        System.out.println(localTime);
        System.out.println(LocalTime.now());
    }
%>

<html>
<head>
    <title>$Title$</title>
    <%@ include file="header.jsp" %>
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="http://localhost:8080/?language=en_US">English </a></li>
                <li><a href="http://localhost:8080/?language=pl_PL">Polski</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container" style="margin-top: 20px">

    <%-- TODO Hello World i aktualna godzina--%>

    <%= JspUtil.upperCase("Hello World")%>
    <%= LocalTime.now()%>
    <%= new java.util.Date()%>

    <%-- TODO Deklaracje --%>
        <fmt:message key = "label.actual.date"></fmt:message>
    <%= LocalDate.now()%><br>
    25 razy 4 to:
    <%= 25 * 4%><br>
    Czy 50 jest mniejsze od 21?
    <%= 50 < 21%><br>

    <%-- TODO zmienne zdefiniowane --%>

    Przeglądarka użytkonika:
    <%=request.getHeader("User-Agent")%>
    <br/>
    Język zapytania:
    <%=request.getLocale()%>
    <br/>
    Lista wszystkich nagłówków zapytania
        <ol>
        <%
            Enumeration<String> enumeration = request.getHeaderNames();
            while(enumeration.hasMoreElements()){
                String name = enumeration.nextElement();
                out.println("<li>"+ name+ " : "+request.getHeader(name)+"</li>");
            }
        %></ol>


    <%-- TODO Obsługa formy --%>
    <%-- TODO Dodać <select> checkbox i radiobutton --%>

    <div class="container" style="margin-top: 20px">
        <form action="client.jsp">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input  type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                       placeholder="Enter email" name="email">
                <small id="emailHelp" class="form-text text-muted">Nigdy nie udostepnimy Twojego e-maila</small>
            </div>
            <div class="form-group">
                <label for="imie">Imie</label>
                <input required name="imie" type="text" class="form-control" id="imie" placeholder="Imie">
            </div>
            <div class="form-group">
                <label for="nazwisko">Nazwisko</label>
                <input name="nazwisko" type="text" class="form-control" id="nazwisko" placeholder="Nazwisko">
            </div>
            <div class="form-group">
                 <select name="payment"  id="">
                     <option>Cash</option>
                     <option>Debit Card</option>
                     <option>Blink</option>
                 </select>
            </div>


            <div class="form-group">
                <input type="radio" name="sex" value="Other">Other
                <input type="radio" name="sex" value="Mele">Mele
                <input type="radio" name="sex" value="Female">Female

            </div>
            <div class="form-group">
                <input type="checkbox" name="zgoda" value="true">Zgoda.
                <input type="checkbox" name="zgoda" value="false">Brak zgody.
            </div>
            <div class="form-group">
                <label for="sel1">Wybierz państwo:</label>
                <select class="form-control" name="panstwo" id="sel1">
                   <c:forEach items="${list2}" var="lang">
                       <option>${lang}</option>
                   </c:forEach>
                    <%--<%--%>
                   <%--for(int i=0;i<list2.size();i++){--%>
                       <%--out.println("<option>" + list2.get(i) + "</option>");--%>
                   <%--}--%>
                   <%--%>--%>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>



    <%@ include file="footer.jsp" %>
</div>
</body>
</html>
