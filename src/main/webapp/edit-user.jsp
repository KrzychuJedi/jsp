<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit user</title>
</head>
<body>

<form action="/user-edit" method="post">
    <input type="text" name="id" value="${user.id}" hidden>
    <br>
    Name <input type="text" name="name" value="${user.firstName}">
    <br>
    Lastname <input type="text" name="lastname" value="${user.lastName}">
    <br>
    Phone <input type="number" name="phone" value="${user.phone}">
    <br>
    Present
    yes <input type="radio" name="present" value="true" ${user.present ? 'checked' : ''}>
    no <input type="radio" name="present" value="false" ${user.present ? '' : 'checked' }>

    <button type="submit">Send</button>

</form>

</body>
</html>
