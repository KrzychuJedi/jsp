<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: krzysztof.gonia
  Date: 9/11/2017
  Time: 8:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ShowUserList</title>
</head>
<body>
<a href="/adduser.jsp">Add user </a>
<table>
    <tr>
        <th>firstName</th>
        <th>lastName</th>
        <th>phone</th>
        <th>present</th>
        <th>EDIT</th>
        <th>DELETE</th>
    </tr>

        <c:forEach items="${userList}" var="user">
            <tr>
                <td>
                    ${user.firstName}
                </td>
                <td>
                    ${user.lastName}
                </td>
                <td>
                    ${user.phone}
                </td>
                <td>
                    ${user.present}
                </td>
                <td>
                    <a href="/userdata?id=${user.id}" >Select</a>
                </td>
                <td>
                    <form method="post" action="/deleteuser">
                       <input type="text" name="id" value="${user.id}" hidden>
                        <button type="submit">Delete</button>

                    </form>
                </td>
            </tr>
        </c:forEach>

<form method="post" action="/logoutUser">
    <input type="submit" value="logout">
</form>


</table>



</body>
</html>
