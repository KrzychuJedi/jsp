<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<c:set var="data" value="Wroclove"></c:set>
dlugosc znakow ${data} to ${fn:length(data)}
<br>
nasz tekst duzymi literami to: ${fn:toUpperCase(data)}
<br>
czy nasz String zaczyna sie od 'Wro'?: ${fn:startsWith(data,"Wro")}
<c:set var="cities" value="Chicago,Wladywostok,Ulan-Bator,Las Vegas"></c:set>
<c:set var="citiesArray" value="${fn:split(cities,',')}"></c:set>
<br>
Wszystkie miasta: <c:forEach items="${citiesArray}" var="city">
    ${city}
    <br>
</c:forEach>
<c:set var="mergeCities" value="${fn:join(citiesArray,'.')}"></c:set>
Polaczone miasta: ${mergeCities}

</body>
</html>
