<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.sda.jsp.dto.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%
List<User> users = new ArrayList<>();
users.add(new User("Margor","Gormar",6666666,true));
users.add(new User("Bartek","Berman",6666661,false));
users.add(new User("Daniel","GriGri",6666644,true));
users.add(new User("Tymon","Tymanski",6666621,false));

    pageContext.setAttribute("users", users);
%>

<html>
<head>
    <title>Title</title>
    <style>
        table, tr, td, th {
            border: 1px solid black;
        }
    </style>
</head>
<body>
<table >
    <tr >
        <th>Name</th>
        <th>Surname</th>
        <th>PhoneNr</th>
        <th>Is Present</th>
    </tr>

    <c:forEach items="${users}" var="user">
    <tr>
        <td>${user.firstName}</td>
        <td>${user.lastName}</td>
        <td>${user.phone}</td>
        <td>
        <c:choose>
            <c:when test="${user.present}">
                +
            </c:when>
            <c:otherwise>
                -
            </c:otherwise>
        </c:choose>
        </td>
    </tr>

    </c:forEach>


</table>

</body>
</html>
