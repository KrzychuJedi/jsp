<%@tag description="User Page template" pageEncoding="UTF-8"%>
<%@tag import="org.sda.jsp.dto.User" %>
<%@attribute name="user" required="true" type="org.sda.jsp.dto.User"%>

First Name: ${user.firstName} <br/>
Last Name: ${user.lastName} <br/>
Phone: ${user.phone}<br/>